package kamkrue.pisupa.lab11;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;

public class TetrisPiece {
	// Indicate the current Tetromino shape of this piece
	private Tetromino pieceShape;
	private int posX, posY;
	private Color color;

	private int coordinates[][]; // coordinate of shape contains 4 points.

	/*
	 * COORDINATE_TABLE provides the coordinate all four squares that compose
	 * into one Tetromino.
	 */
	private final int[][][] COORDINATE_TABLE = new int[][][] { { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } }, // no
																											// shape
			{ { -1, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 } }, // S shape
			{ { -1, 0 }, { 0, 0 }, { 0, 1 }, { 1, 1 } }, // Z shape
			{ { -2, 0 }, { -1, 0 }, { 0, 0 }, { 1, 0 } }, // I shape
			{ { -1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 1 } }, // T shape
			{ { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 } }, // O shape
			{ { -1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 2 } }, // L shape
			{ { -1, 0 }, { -1, 1 }, { -1, 2 }, { 0, 0 } } // J shape
	};

	public static final Color NO_SHAPE_COLOR = new Color(0, 0, 0); // no shape
	public static final Color S_SHAPE_COLOR = new Color(0, 255, 0); // S shape
	public static final Color Z_SHAPE_COLOR = new Color(255, 0, 0); // Z shape
	public static final Color I_SHAPE_COLOR = new Color(0, 255, 255); // I shape
	public static final Color T_SHAPE_COLOR = new Color(170, 0, 255); // T shape
	public static final Color O_SHAPE_COLOR = new Color(255, 255, 0); // O shape
	public static final Color L_SHAPE_COLOR = new Color(255, 165, 0); // L shape
	public static final Color J_SHAPE_COLOR = new Color(0, 0, 255); // J shape
	public static final Color LINE_COLOR = Color.DARK_GRAY; // color of the line
															// for all tetrimino

	// TETRIMINO_COLORS provides all color for each Tetrimino according to their
	// order
	public final static Color TETRIMINO_COLORS[] = { NO_SHAPE_COLOR, S_SHAPE_COLOR, Z_SHAPE_COLOR, I_SHAPE_COLOR,
			T_SHAPE_COLOR, O_SHAPE_COLOR, L_SHAPE_COLOR, J_SHAPE_COLOR };

	// Shape constructor
	public TetrisPiece() {
		coordinates = new int[4][2];// initialize the coordinate of shape
		setShape(randomShape());
		setPosX(TetrisPanel.BOARD_WIDTH / 2);
		setPosY(0);
	}

	// Set the coordinate according to shape
	public void setShapeCoordinate(Tetromino shape) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; ++j) {
				coordinates[i][j] = COORDINATE_TABLE[shape.ordinal()][i][j];
			}
		}
		pieceShape = shape;
	}

	public void setShape(Tetromino shape) {
		setShapeCoordinate(shape);
		pieceShape = shape;
		color = TETRIMINO_COLORS[shape.ordinal()];
	}

	// set x-coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected void setX(int index, int x) {
		coordinates[index][0] = x;
	}

	// set y-coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected void setY(int index, int y) {
		coordinates[index][1] = y;
	}

	protected int getX(int index) {
		return coordinates[index][0];
	}

	protected int getY(int index) {
		return coordinates[index][1];
	}

	public Tetromino getShape() {
		return pieceShape;
	}

	/*
	 * drawTetromino(Graphics2D g2d, int curX, int curY, Tetromino shape) curX -
	 * the position of x coordinate of the drawing shape curY - the position of
	 * y coordinate of the drawing shape These are not pixel. To explain this,
	 * imagine the drwaing panel as matrix with the number of column and row as
	 * BOARD_WIDTH and BOARD_HEIGHT respectively (e.g. 30x20). Note that the
	 * index of matrix start at 0. The curX and curY specify which square in the
	 * matrix do you want to paint. shape - is the shape in enum Tetromino
	 */
	public void draw(Graphics2D g2d) {
		for (int i = 0; i < 4; ++i) {
			int x = posX + getX(i);
			int y = posY + getY(i);
			drawSquare(g2d, x * TetrisPanel.SQUARE_WIDTH, y * TetrisPanel.SQUARE_HEIGHT);
		}
	}

	private void drawSquare(Graphics2D g2d, int x, int y) {
		g2d.setColor(color);
		g2d.fillRect(x, y, TetrisPanel.SQUARE_WIDTH, TetrisPanel.SQUARE_HEIGHT);
		g2d.setColor(TetrisPiece.LINE_COLOR);
		g2d.drawRect(x, y, TetrisPanel.SQUARE_WIDTH, TetrisPanel.SQUARE_HEIGHT);
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public boolean canMoveDown() {
		for (int i = 0; i < 4; ++i) {
			if (getY(i) + getPosY() == TetrisPanel.BOARD_HEIGHT - 1)
				return false;
		}
		return true;
	}

	public boolean canMoveLeft() {
		for (int i = 0; i < 4; ++i) {
			if (getX(i) + getPosX() == 0)
				return false;
		}
		return true;
	}

	public boolean canMoveRight() {
		for (int i = 0; i < 4; ++i) {
			if (getX(i) + getPosX() == TetrisPanel.BOARD_WIDTH - 1)
				return false;
		}
		return true;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void moveLeft() {
		posX--;
	}

	public void moveRight() {
		posX++;
	}

	public void moveDown() {
		posY++;
	}

	public void fallDown() {
		while (canMoveDown()) {
			posY++;
		}
	}

	public void reset() {
		setShape(randomShape());
		setPosY(0);
		setPosX(TetrisPanel.BOARD_WIDTH / 2);
	}

	private Tetromino randomShape() {
		Tetromino temp[] = { Tetromino.S_SHAPE, Tetromino.Z_SHAPE, Tetromino.I_SHAPE, Tetromino.T_SHAPE,
				Tetromino.O_SHAPE, Tetromino.L_SHAPE, Tetromino.J_SHAPE };
		Random rand = new Random();
		return temp[rand.nextInt(7)];
	}

	public void init() {
		setShape(randomShape());
		setPosX(TetrisPanel.BOARD_WIDTH / 2);
		setPosY(0);
	}

}

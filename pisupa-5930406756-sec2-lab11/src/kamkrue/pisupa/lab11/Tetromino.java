package kamkrue.pisupa.lab11;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
public enum Tetromino {
	NO_SHAPE, S_SHAPE, Z_SHAPE, I_SHAPE, T_SHAPE, O_SHAPE, L_SHAPE, J_SHAPE;
}

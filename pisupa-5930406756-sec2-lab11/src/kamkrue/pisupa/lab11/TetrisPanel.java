package kamkrue.pisupa.lab11;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class TetrisPanel extends JPanel implements Runnable, KeyListener {
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 200, HEIGHT = 400;
	public static final int BOARD_WIDTH = 10, BOARD_HEIGHT = 20;
	public static final int SLEEP_TIME = 500;
	public static final int SQUARE_WIDTH = WIDTH / BOARD_WIDTH;
	public static final int SQUARE_HEIGHT = HEIGHT / BOARD_HEIGHT;

	protected int MOVE_LEFT = KeyEvent.VK_LEFT;
	protected int MOVE_RIGHT = KeyEvent.VK_RIGHT;
	protected int FALL_DOWN = KeyEvent.VK_DOWN;

	protected final int MAX_RECT = 600;

	protected Rectangle2D.Double notMovingRec[] = new Rectangle2D.Double[MAX_RECT];
	protected Color notMovingRecColor[] = new Color[MAX_RECT];;
	protected int notMovingRectNum = 0;
	private Thread running;
	protected TetrisPiece curPiece;

	public TetrisPanel() {
		super();
		setBackground(Color.LIGHT_GRAY);
		setSize(new Dimension(WIDTH, HEIGHT));

		curPiece = new TetrisPiece();
		setFocusable(true);

		// You should add listener here.
		addKeyListener(this);

		running = new Thread(this);
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		drawNotMovingRects(g2d);
		curPiece.draw(g2d);
	}

	/**
	 * Runs the game.
	 */
	public void run() {
		while (true) {
			if (curPiece.canMoveDown()) {
				curPiece.moveDown();
			} else {
				pieceNotMoving();
				curPiece.reset();
			}
			repaint();

			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException ignore) {
				// Do nothing
			}

		}
	}

	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	protected void pieceNotMoving() {
		for (int i = 0; i < 4; ++i) {
			int x = curPiece.getPosX() + curPiece.getX(i);
			int y = curPiece.getPosY() + curPiece.getY(i);
			notMovingRec[notMovingRectNum] = new Rectangle2D.Double(x * TetrisPanel.SQUARE_WIDTH,
					y * TetrisPanel.SQUARE_HEIGHT, TetrisPanel.SQUARE_WIDTH, TetrisPanel.SQUARE_HEIGHT);
			notMovingRecColor[notMovingRectNum++] = curPiece.getColor();
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// Your task is to complete the code in this area.
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT) {
			if (curPiece.canMoveLeft()) {
				curPiece.moveLeft();
			}

		}
		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (curPiece.canMoveRight()) {
				curPiece.moveRight();
			}

		}
		if (arg0.getKeyCode() == KeyEvent.VK_DOWN) {
			if (curPiece.canMoveDown()) {
				curPiece.fallDown();
			}

		}

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// Do nothing
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// Do nothing
	}

	protected void drawNotMovingRects(Graphics2D g2d) {
		for (int i = 0; i <= notMovingRectNum - 1; i++) {
			g2d.setColor(notMovingRecColor[i]);
			g2d.fill(notMovingRec[i]);
			g2d.setColor(Color.DARK_GRAY);
			g2d.draw(notMovingRec[i]);
		}
		repaint();
	}

}

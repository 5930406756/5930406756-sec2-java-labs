package kamkrue.pisupa.lab8;

/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.Dimension;
		import java.awt.Toolkit;
		import java.awt.event.ActionEvent;
		import java.awt.event.ActionListener;

		import javax.swing.JDialog;
		import javax.swing.JFrame;
		import javax.swing.JOptionPane;
		import javax.swing.SwingUtilities;

		import kamkrue.pisupa.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener {

	private static final long serialVersionUID = -932292542808417510L;

	public PatientFormV4(String titleName) {
		super(titleName);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.setFrameFeatures();
		patientForm4.addListeners();
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void handleOKButton() {
		String text = "Name = " + nameTextField.getText() + " Birthdate = " + brithdateTextField.getText()
				+ " Weight = " + weightTextField.getText() + " Height = " + heightTextField.getText() + "\nGender = "
				+ (female.isSelected() ? "Female" : "Male") + "\nAddress = " + textAddress.getText() + "\nType = "
				+ patientType.getSelectedItem();
		JOptionPane.showMessageDialog(this, text);
	}

	protected void handleCancelButton() {
		nameTextField.setText("");
		brithdateTextField.setText("");
		weightTextField.setText("");
		heightTextField.setText("");
		textAddress.setText("");
	}

	protected void handleGender() {
		final JOptionPane panel = new JOptionPane(
				"Your gender type is now changed to " + (male.isSelected() ? "Male" : "Female"));
		final JDialog dialogGender = panel.createDialog(null, "Gender info");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		dialogGender.setLocation(x + 5, y + h + 20);
		dialogGender.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected void addListeners() {
		cancelButton.addActionListener(this);
		okButton.addActionListener(this);
		patientType.addActionListener(this);
		male.addActionListener(this);
		female.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == patientType) {
			JOptionPane.showMessageDialog(this,
					"Your Patient type is now changed to " + patientType.getSelectedItem());
			//Sets and shows message when user chooses type of Patient.
		} else if (src == male) {
			handleGender();
		} else if (src == female) {
			handleGender();
		}
	}
}

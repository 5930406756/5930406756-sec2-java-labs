package kamkrue.pisupa.lab9;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PatientFormV9 extends PatientFormV8 {

	protected JFileChooser chooser;
	protected JFileChooser save;

	private static final long serialVersionUID = -8117472057214054657L;

	public PatientFormV9(String string) {
		super(string);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV9 patientForm9 = new PatientFormV9("Patient Form V9");
		patientForm9.addComponents();
		patientForm9.addListeners();
		patientForm9.setFrameFeatures();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object click = e.getSource();
		chooser = new JFileChooser();
		if (click == super.open) {
			int i = chooser.showOpenDialog(this);
			if (i == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				JOptionPane.showMessageDialog(this, "You have opened file " + file.getName());
			} else {
				JOptionPane.showMessageDialog(this, "Open command is cancelled");
			}

		} else

		if (click == super.save) {
			this.save = new JFileChooser();
			int i = this.save.showSaveDialog(this);
			if (i == JFileChooser.APPROVE_OPTION) {
				File nameFile = this.save.getSelectedFile();
				JOptionPane.showMessageDialog(this, "You have saved file " + nameFile.getName());
			} else {
				JOptionPane.showMessageDialog(this, "Save command is cancelled");
			}
		} else

		if (click == super.customColor) {
			Color setColor = textAddress.getForeground();
			Color newColor = JColorChooser.showDialog(null, "Select a color", setColor);
			textAddress.setForeground(newColor);
			nameTextField.setForeground(newColor);
			brithdateTextField.setForeground(newColor);
			weightTextField.setForeground(newColor);
			heightTextField.setForeground(newColor);
		} else if (click == super.exit) {
			System.exit(0);
		}
	}

	protected void addListeners() {
		super.addListeners();
		super.customColor.addActionListener(this);
	}
}
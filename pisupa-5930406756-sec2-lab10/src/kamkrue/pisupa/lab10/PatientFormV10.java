package kamkrue.pisupa.lab10;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import kamkrue.pisupa.lab4.Gender;
import kamkrue.pisupa.lab4.Patient;
import kamkrue.pisupa.lab9.PatientFormV9;

public class PatientFormV10 extends PatientFormV9 {

	private static final long serialVersionUID = 1L;

	protected JMenu dataMenu = new JMenu("Data");
	protected JMenuItem displayMenuItem = new JMenuItem("Display");
	protected JMenuItem sortMenuItem = new JMenuItem("Sort");
	protected JMenuItem searchMenuItem = new JMenuItem("Search");
	protected JMenuItem removeMenuItem = new JMenuItem("Remove");
	protected ArrayList<Patient> patientArray = new ArrayList<Patient>();
	protected Patient newContact;

	public PatientFormV10(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV10 patientForm10 = new PatientFormV10("Patient Form V10");
		patientForm10.addComponents();
		patientForm10.addListeners();
		patientForm10.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == okButton) {
			addPatient();
		} else if (src == displayMenuItem) {
			displayPatients();
		}
	}

	public void addPatient() {
		Gender gender;
    if (male.isSelected()){
    	gender = Gender.MALE;
    }else{
    	gender = Gender.FEMALE;
    }
    Double weight = Double.parseDouble(weightTextField.getText());
    int height = Integer.parseInt(heightTextField.getText());
    newContact = new Patient(nameTextField.getText(), brithdateTextField.getText(),gender,weight,height);
    patientArray.add(newContact);
    }

	public void displayPatients() {
		String patient = "";
		int countList = patientArray.size();
		for (int i =0;i<countList;i++){
			patient +=(i+1)+ ": Patient ["+ patientArray.get(i).getName() + "," + patientArray.get(i).getBirthdate() + "," + patientArray.get(i).getGender()+ "," + patientArray.get(i).getWeight()+" kg.,"+ patientArray.get(i).getHeight()+"cm.]\n";
			//patient +=(i+1)+":"+ newContact.toString() + "\n";
		}

		JOptionPane.showMessageDialog(this, patient);
	}

	public void addComponents(){
		 super.addComponents();
		 dataMenu.add(displayMenuItem);
		 dataMenu.add(sortMenuItem);
		 dataMenu.add(searchMenuItem);
		 dataMenu.add(removeMenuItem);
		 menu.add(dataMenu);
	}
	public void addListeners(){
		super.addListeners();
		 displayMenuItem.addActionListener(this);
	}
	
}

package kamkrue.pisupa.lab10;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import kamkrue.pisupa.lab4.Patient;
import kamkrue.pisupa.lab10.PatientFormV11;;

public class PatientFormV12 extends PatientFormV11 {

	private static final long serialVersionUID = -5453464626739383326L;

	public PatientFormV12(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV12 patientForm12 = new PatientFormV12("Patient Form V12");
		patientForm12.addComponents();
		patientForm12.addListeners();
		patientForm12.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == searchMenuItem) {
			String searchName = JOptionPane.showInputDialog("Please enter the patient name");
			searchPatient(searchName);
		}
		if (src == removeMenuItem) {
			String removeName = JOptionPane.showInputDialog("Please enter the patient name");
			removePatient(removeName);
		}
	}

	protected void removePatient(String removeName) {
		Patient p;
		if ((p = searchPatient(removeName)) != null) {
			patientArray.remove(p);
			displayPatients();
		}

	}

	protected Patient searchPatient(String searchName) {
		for (int i = 0; i < patientArray.size(); i++) {
			Patient p = patientArray.get(i);
			if (searchName.equals(p.getName())) {
				JOptionPane.showMessageDialog(null, p);
				return p;
			}
		}
		JOptionPane.showMessageDialog(null, searchName + " is not found");
		return null;
	}

	public void addListeners() {
		super.addListeners();
		searchMenuItem.addActionListener(this);
		removeMenuItem.addActionListener(this);
	}
}

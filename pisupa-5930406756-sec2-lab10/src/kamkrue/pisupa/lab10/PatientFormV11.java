package kamkrue.pisupa.lab10;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.SwingUtilities;

import kamkrue.pisupa.lab4.Patient;
import kamkrue.pisupa.lab10.PatientFormV10;

public class PatientFormV11 extends PatientFormV10 {
	private static final long serialVersionUID = 1L;

	public PatientFormV11(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV11 patientForm11 = new PatientFormV11("Patient Form V11");
		patientForm11.addComponents();
		patientForm11.addListeners();
		patientForm11.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == sortMenuItem) {
			sortPatients();
			displayPatients();
		}
	}

	protected void sortPatients() {
		for (int i = 0; i < patientArray.size(); i++) {
			Collections.sort(patientArray, new Comparator<Patient>() {
				public int compare(Patient patient1, Patient patient2) {
					if (patient1.getWeight() < patient2.getWeight()) {
						Collections.swap(patientArray, patientArray.indexOf(patient1), patientArray.indexOf(patient2));
					}
					return 0;
				}
			});
		}

	}

	public void addComponents() {
		super.addComponents();
	}

	public void addListeners() {
		super.addListeners();
		super.sortMenuItem.addActionListener(this);
	}
}

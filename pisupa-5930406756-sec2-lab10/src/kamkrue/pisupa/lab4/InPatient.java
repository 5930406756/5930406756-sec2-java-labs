package kamkrue.pisupa.lab4;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.time.LocalDate;

public class InPatient extends Patient {

	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	private LocalDate admitDate;
	private LocalDate dischargeDate;

	public LocalDate getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
	}

	public LocalDate getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);
	}

	public InPatient(String name, String birthdate, Gender gender, double weight, int height, String admitDate,
			String dischargeDate) {
		super(name, birthdate, gender, weight, height);
		this.name = name;
		this.birthdate = LocalDate.parse(birthdate, germanFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);
	}

	@Override
	public String toString() {
		return "InPatient [ " + name + ", " + birthdate + ", " + gender + ", " + weight + " kg., " + height
				+ " cm. admitDate=" + admitDate + ", dischargeDate=" + dischargeDate + "]";
	}

}

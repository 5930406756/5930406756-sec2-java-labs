package kamkrue.pisupa.lab4;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Patient {
	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	public DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);

	public Patient(String name, String birthdateStr, Gender gender, double weight, int height) {
		this.name = name;
		this.birthdate = LocalDate.parse(birthdateStr, germanFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Patient [" + name + "," + birthdate + "," + gender + "," + weight + " kg.," + height + " cm.]";
	}
}

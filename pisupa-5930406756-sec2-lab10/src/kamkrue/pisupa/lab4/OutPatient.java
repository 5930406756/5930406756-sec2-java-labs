package kamkrue.pisupa.lab4;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class OutPatient extends Patient {
	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	private LocalDate visitDate;
	static String hospitalName = "Srinakarin";
	public DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);

	public OutPatient(String name, String birthdate, Gender gender, double weight, int height) {

		super(name, birthdate, gender, weight, height);
		this.name = name;
		this.gender = gender;
		this.birthdate = LocalDate.parse(birthdate, germanFormatter);
		this.height = height;
		this.weight = weight;
	}

	public OutPatient(String name, String birthdate, Gender gender, double weight, int height, String visitDate) {

		super(name, birthdate, gender, weight, height);

		this.name = name;
		this.gender = gender;
		this.birthdate = LocalDate.parse(birthdate, germanFormatter);
		this.height = height;
		this.weight = weight;
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);

	}

	public LocalDate getDate() {
		return visitDate;
	}

	public long getDaysCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {

		long daysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);

		return daysBetween;
	}

	public void setVisitDate(String setVisitDate) {
		this.visitDate = LocalDate.parse(setVisitDate, germanFormatter);

	}

	public LocalDate getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(LocalDate visitDate) {
		this.visitDate = visitDate;
	}

	public void displayDaysBetween(OutPatient piti) {

		long dayBetween = getDaysCountBetweenDates(piti.getDate(), visitDate);
		System.out.println("Chujai visited after Piti for  " + dayBetween + " days.");
	}

	@Override
	public String toString() {
		return "OutPatient [" + name + ", " + "birthdate = " + birthdate + ", " + gender + ", " + weight + " kg., "
				+ height + " cm." + "visit date = " + visitDate + "]";
	}
}

package kamkrue.pisupa.lab4;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
public enum Gender {
	MALE, FEMALE;
}

package kamkrue.pisupa.lab8;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class PatientFormV5 extends PatientFormV4 implements ActionListener {
	private static final long serialVersionUID = 5922644669663900044L;
	protected JMenu colorMenuConfig, sizeMenuConfig;
	protected JMenuItem blue, red, green, customColor, sixTeen, twenty, twentyFour, customSize;
	protected Color color;
	protected int textSize;

	public PatientFormV5(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV5 patientFormV5 = new PatientFormV5("Patient Form V5");
		patientFormV5.addComponents();
		patientFormV5.setFrameFeatures();
		patientFormV5.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		menuConfig.removeAll();
		colorMenuConfig = new JMenu("Color");
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		customColor = new JMenuItem("Custom...");
		menuConfig.add(colorMenuConfig);
		colorMenuConfig.add(blue);
		colorMenuConfig.add(green);
		colorMenuConfig.add(red);
		colorMenuConfig.add(customColor);

		sizeMenuConfig = new JMenu("Size");
		sixTeen = new JMenuItem("16");
		twenty = new JMenuItem("20");
		twentyFour = new JMenuItem("24");
		customSize = new JMenuItem("Custom...");
		menuConfig.add(sizeMenuConfig);
		sizeMenuConfig.add(sixTeen);
		sizeMenuConfig.add(twenty);
		sizeMenuConfig.add(twentyFour);
		sizeMenuConfig.add(customSize);
	}

	protected void addListeners() {
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		sixTeen.addActionListener(this);
		twenty.addActionListener(this);
		twentyFour.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		JMenuItem src = (JMenuItem) e.getSource();
		if (src == blue) {
			nameTextField.setForeground(Color.BLUE);
			brithdateTextField.setForeground(Color.BLUE);
			weightTextField.setForeground(Color.BLUE);
			heightTextField.setForeground(Color.BLUE);
			textAddress.setForeground(Color.BLUE);
		} else if (src == green) {
			nameTextField.setForeground(Color.GREEN);
			brithdateTextField.setForeground(Color.GREEN);
			weightTextField.setForeground(Color.GREEN);
			heightTextField.setForeground(Color.GREEN);
			textAddress.setForeground(Color.GREEN);
		} else if (src == red) {
			nameTextField.setForeground(Color.RED);
			brithdateTextField.setForeground(Color.RED);
			weightTextField.setForeground(Color.RED);
			heightTextField.setForeground(Color.RED);
			textAddress.setForeground(Color.RED);
		} else if (src == sixTeen) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 16));
		} else if (src == twenty) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 20));
		} else if (src == twentyFour) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 24));
		}
	}
}

package kamkrue.pisupa.lab4;
/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import kamkrue.pisupa.lab4.Gender;
import kamkrue.pisupa.lab4.OutPatient;

public class KhonKaenPatientsV2 {
	public static void main(String[] aargs) {
		OutPatient chujai = new OutPatient("Chujai", "02.06.1995", Gender.FEMALE, 52.7, 150);
		OutPatient piti = new OutPatient("Piti", "13.08.1995", Gender.MALE, 52.7, 150, "21.01.2017");
		chujai.setVisitDate("25.01.2017");
		System.out.println(chujai);
		System.out.println(piti);
		chujai.displayDaysBetween(piti);
		System.out.println("Both of them went to " + OutPatient.hospitalName + " hospital.");
	}
}
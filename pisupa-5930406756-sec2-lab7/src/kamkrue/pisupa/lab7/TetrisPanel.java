package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import javax.swing.JPanel;

public class TetrisPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	final static int WIDTH = 600;
	final static int HEIGHT = 400;
	final static int  BOARD_WIDTH= 30;
	final static int BOARD_HEIGHT = 20;
	final static int  SQUARE_WIDTH = WIDTH / BOARD_WIDTH;
	final static int SQUARE_HEIGHT = HEIGHT / BOARD_HEIGHT;
	final static String string = "Tetris";
	final static String TITLE_PANEL = "Tetris";
	final static Font TITLE_FONT = new Font("serif", Font.BOLD, 30);
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}
	
	public TetrisPanel () {
		super () ;
		setBackground(Color.LIGHT_GRAY);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		setPreferredSize(getPreferredSize());
		Graphics2D s = (Graphics2D) g;
		g.setColor(Color.RED);
		g.setFont(TITLE_FONT);
		int n = g.getFontMetrics().stringWidth(TITLE_PANEL);
		g.drawString(TITLE_PANEL, (WIDTH - n) / 2, HEIGHT / 4);
		
		TetrisShape shapeZ = new TetrisShape(Tetrimino.Z_SHAPE);
		TetrisShape shapeI = new TetrisShape(Tetrimino.I_SHAPE);
		TetrisShape shapeJ = new TetrisShape(Tetrimino.J_SHAPE);
		TetrisShape shapeO = new TetrisShape(Tetrimino.O_SHAPE);
		TetrisShape shapeS = new TetrisShape(Tetrimino.S_SHAPE);
		TetrisShape shapeT = new TetrisShape(Tetrimino.T_SHAPE);
		TetrisShape shapeL = new TetrisShape(Tetrimino.L_SHAPE);
		TetrisShape[] shape= new TetrisShape[] {shapeZ, shapeS,  shapeI, shapeT, shapeO, shapeL, shapeJ};
		
		int currentPosX = 2 * SQUARE_WIDTH;
		int currentPosY = 8 * SQUARE_HEIGHT;
		
		for(int j = 0 ; j < 7 ; j++){
			for(int i = 0 ; i < 4 ; i++){
				int currentBlockPosX = currentPosX + (shape[j].coordinateX[i] *SQUARE_WIDTH);
				int currentBlockPosY = currentPosY + (shape[j].coordinateY[i] * SQUARE_HEIGHT);
				s.setColor(shape[j].getColor());
				s.fillRect(currentBlockPosX, currentBlockPosY, SQUARE_WIDTH,SQUARE_HEIGHT);
				s.setColor(Color.BLACK);
				s.drawRect(currentBlockPosX, currentBlockPosY, SQUARE_WIDTH,SQUARE_HEIGHT);
			}
			currentPosX +=  SQUARE_WIDTH * 4;
		}
	}
}

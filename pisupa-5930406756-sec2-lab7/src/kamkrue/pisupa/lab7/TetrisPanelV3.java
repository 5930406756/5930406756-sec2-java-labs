package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;

public class TetrisPanelV3 extends TetrisPanelV2 {

	private static final long serialVersionUID = -887801233266548384L;

	protected final int RECT_WIDTH = 60,RECT_HEIGHT = 40;
	Color newcolor;
	int panelheight = this.getSize().height;
	ArrayList<Color> colorBlock = new ArrayList<>();
	ArrayList<Rectangle2D.Double> finalPosit = new ArrayList<>();

	public TetrisPanelV3() {
		running = new Thread(this);
		running.start();
	}

	@Override
	public void paint(Graphics graphic) {
		super.paintComponent(graphic);
		this.setBackground(Color.white);
		for (int i = 0; i < finalPosit.size(); i++) {
			if (i == 0) {
			} 
			else {
				graphic.setColor(colorBlock.get(i - 1));
				((Graphics2D) graphic).fill(finalPosit.get(i));
				graphic.setColor(Color.darkGray);
				((Graphics2D) graphic).draw(finalPosit.get(i));
				graphic.setColor(colorBlock.get(i));
				graphic.fillRect(randomPoX, positionY, RECT_WIDTH * 2, RECT_HEIGHT * 2);
				graphic.setColor(Color.darkGray);
				graphic.drawRect(randomPoX, positionY, RECT_WIDTH * 2, RECT_HEIGHT * 2);
			}
		}
	}

	public void run() {
		while (true) {
			newcolor = randColor();
			if (positionY+RECT_HEIGHT+20  == HEIGHT && finalPosit.size() == colorBlock.size()) {
				finalPosit.add(new Rectangle2D.Double(randomPoX, HEIGHT-RECT_HEIGHT-20, RECT_WIDTH * 2,
						RECT_HEIGHT * 2));
				colorBlock.add(newcolor);
				randomPoX = (int) (Math.random() * (600 - 0)) + 0;
				positionY = 0;
			}
			
			positionY++;
			repaint();

			try {
				Thread.sleep(10);
			} 
			
			catch (InterruptedException ex) {
			}
		}
	}

	public Color randColor() {
		return new Color((int) (Math.random() * (255 - 0)) + 0, (int) (Math.random() * (255 - 0)) + 0,
				(int) (Math.random() * (255 - 0)) + 0);
	}
}

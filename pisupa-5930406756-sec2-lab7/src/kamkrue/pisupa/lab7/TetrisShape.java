package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.Color;

public class TetrisShape {
	Tetrimino shape;
	int coordinateX[] = new int[4];
	int coordinateY[] = new int[4];
	Color color;

	public Tetrimino getShape() {
		return shape;
	}

	public TetrisShape(Tetrimino shape2) {
		this.shape = shape2;
		setShapeColor(shape2);
		setShapeCoordinate(shape);
	}

	public void setShape(Tetrimino shape) {
		this.shape = shape;
	}

	private void setShapeColor(Tetrimino shape) {
		switch (shape) {
		case Z_SHAPE:
			color = new Color(255, 0, 0);
			break;
		case S_SHAPE:
			color = new Color(0, 255, 0);
			break;
		case I_SHAPE:
			color = new Color(0, 255, 255);
			break;
		case T_SHAPE:
			color = new Color(170, 0, 255);
			break;
		case O_SHAPE:
			color = new Color(255, 255, 0);
			break;
		case L_SHAPE:
			color = new Color(255, 165, 0);
			break;
		case J_SHAPE:
			color = new Color(0, 0, 255);
			break;
		}
	}

	private void setShapeCoordinate(Tetrimino shape) {
		switch (shape) {
		case Z_SHAPE:
			coordinateX = new int[] { 1, 1, 0, 0 };
			coordinateY = new int[] { 0, 1, 1, 2 };
			break;
		case S_SHAPE:
			coordinateX = new int[] { 1, 2, 1, 2 };
			coordinateY = new int[] { 0, 1, 1, 2 };
			break;
		case I_SHAPE:
			coordinateX = new int[] { 1, 1, 1, 1 };
			coordinateY = new int[] { 0, 1, 2, 3 };
			break;
		case T_SHAPE:
			coordinateX = new int[] { 0, 1, 2, 1 };
			coordinateY = new int[] { 1, 1, 1, 2 };
			break;
		case O_SHAPE:
			coordinateX = new int[] { 1, 2, 1, 2 };
			coordinateY = new int[] { 1, 1, 2, 2 };
			break;
		case L_SHAPE:
			coordinateX = new int[] { 0, 1, 1, 1 };
			coordinateY = new int[] { 0, 0, 1, 2 };
			break;
		case J_SHAPE:
			coordinateX = new int[] { 1, 2, 1, 1 };
			coordinateY = new int[] { 0, 0, 1, 2 };
			break;
		}
	}

	public Color getColor() {
		return color;
	}
}

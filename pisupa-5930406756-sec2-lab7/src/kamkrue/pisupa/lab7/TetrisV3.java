package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import javax.swing.SwingUtilities;

public class TetrisV3 extends TetrisV2 {

	private static final long serialVersionUID = 1L;

	public TetrisV3(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		TetrisV3 tetrisV3 = new TetrisV3("Rectangles Dropping V2");
		tetrisV3.addComponents();
		tetrisV3.setFrameFeatures();
	}

	@Override
	protected void addComponents() {
		TetrisPanelV3 panel = new TetrisPanelV3();
		add(panel);
	}
}

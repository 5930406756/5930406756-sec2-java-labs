package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
public enum Tetrimino {
	NO_SHAPE, I_SHAPE, J_SHAPE, L_SHAPE, O_SHAPE, S_SHAPE, T_SHAPE, Z_SHAPE
}

package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;

public class TetrisPanelV2 extends TetrisPanel implements Runnable {
		private static final long serialVersionUID = 8072540061737053179L;

		Thread running;
		int positionY = 0,randomPoX,num = 0;
		final int SPEED = 25;

		public TetrisPanelV2() {
			running = new Thread(this);
			running.start();
		}

		@Override
		public void paint(Graphics graphic) {
			super.paintComponent(graphic);
			setBackground(Color.black);
			graphic.setColor(Color.YELLOW);
			graphic.fillRect(randomPoX, positionY, 50, 50);
			repaint();
		}

		public void run() {
			while (true) {
				if (num == 0) {
					randomPoX = super.WIDTH / 2;
					positionY = 0;
					num++;
				}
				positionY++;
				if (positionY > 400) {
					randomPoX = (int) (Math.random() * (600 - 0)) + 0;
					positionY = 0;
					repaint();
				}

				try {
					Thread.sleep(SPEED);
				} 
				
				catch (InterruptedException ex) {
				}
			}
		}
}

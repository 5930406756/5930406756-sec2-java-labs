package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import javax.swing.*;


public class Tetris extends JFrame {

	private static final long serialVersionUID = 2224672607261978769L;

	public Tetris(String title) {
		super(title);
	}

	protected void addComponents() {
		TetrisPanel mainPanel = new TetrisPanel();
		setContentPane(mainPanel);
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		Tetris tetrisGame = new Tetris("CoE Tetris Game");
		tetrisGame.addComponents();
		tetrisGame.setFrameFeatures();
	}
}

package kamkrue.pisupa.lab7;
/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import javax.swing.SwingUtilities;

public class TetrisV2 extends Tetris {

	private static final long serialVersionUID = 2280800612215419899L;

	public TetrisV2(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		TetrisV2 tetrisV2 = new TetrisV2("Rectangles Dropping");
		tetrisV2.addComponents();
		tetrisV2.setFrameFeatures();
	}

	@Override
	protected void addComponents() {
		TetrisPanelV2 panel = new TetrisPanelV2();
		add(panel);
	}
}

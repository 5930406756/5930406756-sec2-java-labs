package kamkrue.pisupa.lab3;
/**
 * GuessNumberGameV2 which is an improved version of the GuessNumberGame. You can choose the minimum and maximum value of the answer. The order of input of min and max may can be in any order.
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.util.Scanner;

public class GuessNumberGameV2 {
	static int randomNum, min, max;
	static String play = "y", check = "";

	public static void main(String[] args) {
		genAnswer();
		playGame();
		playAgain();
		while (check.equalsIgnoreCase(play)) {
			genAnswer();
			playGame();
			playAgain();
		}
		System.exit(0);

	}

	public static void genAnswer() {
		Scanner input = new Scanner(System.in);

		System.out.print("Enter min and max of random numbers: ");
		min = input.nextInt();
		max = input.nextInt();
		randomNum = min + (int) (Math.random() * ((max - min) + 1));

	}

	public static void playGame() {
		Scanner input1 = new Scanner(System.in);
		System.out.print("Enter the number of possible guess: ");
		int numGuess = input1.nextInt();
		for (int numberGuess = numGuess; numberGuess > 0; numberGuess--) {
			System.out.println("Number of remaining guess is " + numberGuess);
			System.out.print("Enter a guess: ");
			int num = input1.nextInt();
			if ((num < min || num > max) && numberGuess != 1) {
				System.out.println("Number must be in " + min + " and " + max);
			} else if (randomNum > num) {
				System.out.println("Higher!"); // �Higher!�: if the number entered is smaller than the answer. 
			} else if (randomNum == num) {
				System.out.println("Correct!"); //  �Correct!�: if a number entered is the same as the answer.
				break;
			} else if (randomNum < num) {
				System.out.println("Lower!"); // �Lower!�: if the number entered is higher than the answer.
			} else if (numberGuess == 1) {
				System.out.println("You ran out of guesses. The number was " + randomNum);
				break;
			}
		}
	}

	public static void playAgain() { // After the game ends, the program give an option to play again.
		Scanner input2 = new Scanner(System.in);
		System.out.print("Want to play again? (\"Y\" or \"y\") : ");
		check = input2.next();

	}

}

package kamkrue.pisupa.lab3;
/**
 * GuessNumberGame is game guesses number between 0 to 100. The answer is randomly generated then a user enter a number via the console . And  the game ends when user guess the answer correctly or the user guess the number incorrectly seven times. 
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.util.Scanner;
public class GuessNumberGame {

	public static void main(String[] args) {
	    int randomNum = 0+(int)(Math.random()*((100-0) + 1));
		Scanner input = new Scanner(System.in);
		for(int count = 7; count > 0; count--){
			System.out.println("Number of remaining guess is " + count);
			System.out.print("Enter a guess: ");
			int num = input.nextInt();
			if (randomNum > num) {
			System.out.println("Higher!"); // �Higher!�: if the number entered is smaller than the answer.
		} else if (randomNum < num) {
			System.out.println("Lower!"); // �Lower!�: if the number entered is higher than the answer.
		} else {
			System.out.println("Correct!"); //  �Correct!�: if a number entered is the same as the answer.
			System.exit(0);
		} 
		}System.out.println("You ran out of guesses. The number was " + randomNum);
		System.exit(0);
	}
}

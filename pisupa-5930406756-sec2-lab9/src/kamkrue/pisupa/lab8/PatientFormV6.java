package kamkrue.pisupa.lab8;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.security.KeyStore.ProtectionParameter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class PatientFormV6 extends PatientFormV5 {

    private static final long serialVersionUID = -1036569868974029977L;
    protected JLabel maneeImage = new JLabel();
    protected JPanel ImagePanel = new JPanel();
    protected JPanel informationPanel = new JPanel();
   
   

    public PatientFormV6(String titleName) {
        super(titleName);

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });

    }

    public static void createAndShowGUI() {
        PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
        patientForm6.addComponents();
        patientForm6.setFrameFeatures();
        patientForm6.addListeners();

    }

    protected void addComponents() {
        super.addComponents(); 
        ImageIcon imageOpen = new ImageIcon("images/openIcon.png");
        ImageIcon imageSave = new ImageIcon("images/saveIcon.png");
        ImageIcon imageExit = new ImageIcon("images/quitIcon.png");
        ImageIcon imageManee = new ImageIcon("images/manee.jpg");

        open.setIcon(imageOpen);
        save.setIcon(imageSave);
        exit.setIcon(imageExit);
        maneeImage.setIcon(imageManee);
        ImagePanel.add(maneeImage);
        ImagePanel.setBorder(new EmptyBorder(30, 30, 30, 30));
        informationPanel.setLayout(new BorderLayout());
        informationPanel.add(ImagePanel, BorderLayout.NORTH);
        add(informationPanel, BorderLayout.NORTH);

    }

}
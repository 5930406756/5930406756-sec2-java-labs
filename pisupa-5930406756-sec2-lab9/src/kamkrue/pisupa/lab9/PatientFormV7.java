package kamkrue.pisupa.lab9;

/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import kamkrue.pisupa.lab8.PatientFormV6;

public class PatientFormV7 extends PatientFormV6 implements ChangeListener {

	private static final long serialVersionUID = 1L;
	protected JSlider slider1, slider2;
	protected JPanel content, centerPanel;
	protected JLabel sliderLabel1, sliderLabel2;

	public PatientFormV7(String titleName) {
		super(titleName);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.setFrameFeatures();
		patientForm7.addListener();
	}

	protected void addComponents() {
		super.addComponents();
		slider1 = new JSlider(0, 200);
		slider2 = new JSlider(0, 200);
		slider1.setMajorTickSpacing(50);
		slider1.setMinorTickSpacing(10);
		slider1.setPaintTicks(true);
		slider1.setLabelTable(slider1.createStandardLabels(50));
		slider1.setPaintLabels(true);
		slider2.setMajorTickSpacing(50);
		slider2.setMinorTickSpacing(10);
		slider2.setPaintTicks(true);
		slider2.setLabelTable(slider2.createStandardLabels(50));
		slider2.setPaintLabels(true);
		sliderLabel1 = new JLabel("Top number: ");
		sliderLabel2 = new JLabel("Bottom number: ");
		content = new JPanel();
		content.setLayout(new GridLayout(2, 2));
		content.setBorder(BorderFactory.createTitledBorder("Blood Pressure"));
		content.add(sliderLabel1);
		content.add(slider1);
		content.add(sliderLabel2);
		content.add(slider2);
		genderAndAddressPanel.add(content, BorderLayout.CENTER);

	}

	public void addListener() {
		super.addListeners();
		slider1.addChangeListener(this);
		slider2.addChangeListener(this);
		cancelButton.addActionListener(this);
		okButton.addActionListener(this);
		patientType.addActionListener(this);
		male.addActionListener(this);
		female.addActionListener(this);

		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		sixTeen.addActionListener(this);
		twenty.addActionListener(this);
		twentyFour.addActionListener(this);
	}

	@Override
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void handleOKButton() {
		String text = "Name = " + nameTextField.getText() + " Birthdate = " + brithdateTextField.getText()
				+ " Weight = " + weightTextField.getText() + " Height = " + heightTextField.getText() + "\nGender = "
				+ (female.isSelected() ? "Female" : "Male") + "\nAddress = " + textAddress.getText() + "\nType = "
				+ patientType.getSelectedItem() + "\nBlood Pressure: " + "Top Number = " + slider1.getValue() + ","
				+ " Buttom Number = " + slider2.getValue();
		JOptionPane.showMessageDialog(this, text);
	}

	public void stateChanged(ChangeEvent event) {
		JSlider src = (JSlider) event.getSource();
		if (!src.getValueIsAdjusting()) {
			int value = src.getValue();
			JOptionPane.showMessageDialog(this,
					(src == slider1 ? "Top number" : "Buttom number") + " of Blood Pressure is " + value);
		}
	}

	protected void handleCancelButton() {
		nameTextField.setText("");
		brithdateTextField.setText("");
		weightTextField.setText("");
		heightTextField.setText("");
		textAddress.setText("");
	}

	protected void handleGender() {
		final JOptionPane panel = new JOptionPane(
				"Your gender type is now changed to " + (male.isSelected() ? "Male" : "Female"));
		final JDialog dialogGender = panel.createDialog(null, "Gender info");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		dialogGender.setLocation(x + 5, y + h + 20);
		dialogGender.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == patientType) {
			JOptionPane.showMessageDialog(this, "Your Patient type is now changed to " + patientType.getSelectedItem());
		} else if (src == male) {
			handleGender();
		} else if (src == female) {
			handleGender();
		}

		if (src == blue) {
			nameTextField.setForeground(Color.BLUE);
			brithdateTextField.setForeground(Color.BLUE);
			weightTextField.setForeground(Color.BLUE);
			heightTextField.setForeground(Color.BLUE);
			textAddress.setForeground(Color.BLUE);
		} else if (src == green) {
			nameTextField.setForeground(Color.GREEN);
			brithdateTextField.setForeground(Color.GREEN);
			weightTextField.setForeground(Color.GREEN);
			heightTextField.setForeground(Color.GREEN);
			textAddress.setForeground(Color.GREEN);
		} else if (src == red) {
			nameTextField.setForeground(Color.RED);
			brithdateTextField.setForeground(Color.RED);
			weightTextField.setForeground(Color.RED);
			heightTextField.setForeground(Color.RED);
			textAddress.setForeground(Color.RED);
		} else if (src == sixTeen) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 16));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 16));
		} else if (src == twenty) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 20));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 20));
		} else if (src == twentyFour) {
			nameTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			brithdateTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			weightTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			heightTextField.setFont(new Font("Sans Serif", Font.BOLD, 24));
			textAddress.setFont(new Font("Sans Serif", Font.BOLD, 24));
		}
	}
}

package kamkrue.pisupa.lab9;
/**
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class PatientFormV8 extends PatientFormV7{


    private static final long serialVersionUID = 1L;

    public PatientFormV8(String title) {
        super(title);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public static void createAndShowGUI() {
        PatientFormV8 patientForm8 = new PatientFormV8("Patient Form V8");
        patientForm8.addComponents();
        patientForm8.addListeners();
        patientForm8.setFrameFeatures();
    }

    @Override
    protected void addComponents() {
        super.addComponents();
        super.menuFile.setMnemonic(KeyEvent.VK_H);
        super.menuItem.setMnemonic(KeyEvent.VK_N);
        super.open.setMnemonic(KeyEvent.VK_O);
        super.save.setMnemonic(KeyEvent.VK_S);
        super.exit.setMnemonic(KeyEvent.VK_X);
        super.menuConfig.setMnemonic(KeyEvent.VK_C);
        super.colorMenuConfig.setMnemonic(KeyEvent.VK_L);
        super.blue.setMnemonic(KeyEvent.VK_B);
        super.green.setMnemonic(KeyEvent.VK_G);
        super.red.setMnemonic(KeyEvent.VK_R);
        super.customColor.setMnemonic(KeyEvent.VK_U);
        super.sizeMenuConfig.setMnemonic(KeyEvent.VK_Z);
        super.sixTeen.setMnemonic(KeyEvent.VK_6);
        super.twenty.setMnemonic(KeyEvent.VK_0);
        super.twentyFour.setMnemonic(KeyEvent.VK_4);
        super.customSize.setMnemonic(KeyEvent.VK_M);
        super.menuItem.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        super.open.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        super.save.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        super.exit.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        super.blue.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
        super.red.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        super.green.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        super.customColor.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
        super.sixTeen.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
        super.twenty.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
        super.twentyFour.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
        super.customSize.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));

    }

    @Override
    protected void addListeners() {
        super.addListeners();
        super.menuFile.addActionListener(this);
        super.menuItem.addActionListener(this);
        super.open.addActionListener(this);
        super.save.addActionListener(this);
        super.exit.addActionListener(this);
        super.menuConfig.addActionListener(this);
        super.colorMenuConfig.addActionListener(this);
        super.sizeMenuConfig.addActionListener(this);
    }
}

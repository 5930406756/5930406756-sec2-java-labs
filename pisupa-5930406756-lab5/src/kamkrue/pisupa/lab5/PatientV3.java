package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import kamkrue.pisupa.lab4.Gender;

public class PatientV3 extends SickPeople {

	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public PatientV3(String name, String birthdate, Gender gender, double weight, int height) {
		this.name = name;
		this.birthdate = LocalDate.parse(birthdate, germanFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, germanFormatter);
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Patient [" + name + ", " + birthdate + ", " + gender + ", " + weight + " kg., " + height + " cm.]";
	}

	@Override
	public void seeDoctor() {
		System.out.println(" go see the doctor.");

	}
}

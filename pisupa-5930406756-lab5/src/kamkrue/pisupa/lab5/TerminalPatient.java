package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import kamkrue.pisupa.lab4.Gender;

public class TerminalPatient extends PatientV2 {
	private String terminalDisease;
	private LocalDate firstdiagnosed;
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public TerminalPatient(String name, String birthdayStr, Gender gender, double weight, int height,
			String terminalDisease, String firstdiagnosed) {
		super(name, birthdayStr, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstdiagnosed = LocalDate.parse(firstdiagnosed, germanFormatter);
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstdiagnosed() {
		return firstdiagnosed;
	}

	public void setFirstdiagnosed(String firstdiagnosed) {
		this.firstdiagnosed = LocalDate.parse(firstdiagnosed, germanFormatter);
	}

	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", " + firstdiagnosed + "], " + super.toString();
	}

	public void patientReport() {
		System.out.println("You have terminal illness");
	}

}

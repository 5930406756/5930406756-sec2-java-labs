package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import kamkrue.pisupa.lab4.Gender;

public class TerminalPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {
	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	private String terminalDisease;
	private LocalDate firstDiagnosed;

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(String firstDiagnosed) {
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}

	public TerminalPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String terminalDisease, String firstDiagnosed) {
		super(name, birthdate, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}

	public void pay(double amount) {
		System.out.println("pay " + amount + " baht for his hospital visit by insurance.");
	}

	public void askPermission(String legalGuardianName) {
		System.out.println("ask " + legalGuardianName + " for permissioin to treat with Chemo.");
	}
}

package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import kamkrue.pisupa.lab4.Gender;

public class VIPPatientV2 extends PatientV3 implements HasInsurance, HasPet {
	private Double totalDonation;
	private String passPhrase;

	public void feedPet() {
		System.out.println("feed the pet.");
	}

	public void playWithPet() {
		System.out.println("pay with pet.");
	}

	public VIPPatientV2(String name, String birthdate, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(name, birthdate, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
		super.setName(encrypt(name));

	}

	public void donate(double donate) {
		totalDonation += donate;

	}

	private String encrypt(String encrypt) {
		char[] encryptArray = encrypt.toCharArray();
		for (int i = 0; i < encrypt.length(); i++) {
			encryptArray[i] = (char) ((encryptArray[i] - 'a' + 7) % 26 + 'a');
		}
		String encryptString = new String(encryptArray);
		return encryptString;
	}

	private String decrypt(String decrypt) {
		char[] encryptArray = decrypt.toCharArray();
		for (int i = 0; i < decrypt.length(); i++) {
			encryptArray[i] = (char) ((encryptArray[i] - 'a' - 7) % 26 + 'a');
			if (encryptArray[i] < 97) {
				encryptArray[i] += 26;
			}
		}
		String encryptString = new String(encryptArray);
		return encryptString;
	}

	public String getVIPName(String passPhrase) {
		if (this.passPhrase.equals(passPhrase)) {
			return decrypt(super.getName());
		} else {
			return "Wrong passphrase, please try again";
		}
	}

	public Double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(Double totalDonation) {
		this.totalDonation = totalDonation;
	}

	@Override
	public String toString() {
		return "TerminalPatient" + "[" + totalDonation + "," + "  Patient [" + super.getName() + ", "
				+ super.getBirthdate() + ", " + super.getGender() + ", " + super.getWeight() + " kg., "
				+ super.getHeight() + " cm." + "]";
	}

	public void patientReport() {
		System.out.println("Your record is private.");

	}

}

package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import kamkrue.pisupa.lab4.*;

public class AccidentPatient extends PatientV2 {

	public AccidentPatient(String name, String birthdateStr, Gender gender, double weight, int height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdateStr, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}

	private String typeOfAccident;
	private boolean isInICU;

	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " + (isInICU ? "In ICU" : "is out of ICU") + "], "
				+ super.toString() + "]";
	}

	public void patientReport() {
		System.out.println("You were in an accident.");
	}
}

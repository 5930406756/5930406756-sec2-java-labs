package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import kamkrue.pisupa.lab4.Gender;

public class VIPPatient extends PatientV2 {
	private double totalDonation;
	private String passPhrase;

	public VIPPatient(String name, String birthdayStr, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdayStr, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;

	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	public double donate(double donate) {
		totalDonation = totalDonation + donate;
		return totalDonation;
	}

	public static String encrypt(String message) {
		String newMessage = "";
		char newCharacter;

		for (int i = 0; i < message.length(); i++) {
			newCharacter = message.charAt(i);
			newCharacter += 7;
			if (newCharacter > 'z') {
				newCharacter -= 26;

			}
			newMessage += newCharacter;
		}
		return newMessage;

	}

	public static String decrypt(String message) {
		String newMessage = "";
		char newCharacter;

		for (int i = 0; i < message.length(); i++) {
			newCharacter = message.charAt(i);
			newCharacter -= 7;
			if (newCharacter < 'a') {
				newCharacter += 26;

			}
			newMessage += newCharacter;
		}
		return newMessage;

	}

	public void setName(String name) {
		super.setName(encrypt(name));
	}

	public String getVIPName(String passPhrase) {
		if (passPhrase.equals(this.passPhrase)) {
			return decrypt(this.getName());
		} else {
			return "Wrong passpharse, please try again";
		}

	}

	public void patientReport() {
		System.out.println("Your record is private");
	}

	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ", " + super.toString() + "]";
	}

}
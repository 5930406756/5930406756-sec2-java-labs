package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import java.time.LocalDate;
import kamkrue.pisupa.lab4.Gender;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {
	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	private String typeOfAccident;
	private boolean isInICU;

	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	public AccidentPatientV2(String name, String birthdate, Gender gender, double weight, int height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthdate, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}

	public void pay() {
		System.out.println(" pay the accident bill with insurrance.");
	}

	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident.");

	}

}

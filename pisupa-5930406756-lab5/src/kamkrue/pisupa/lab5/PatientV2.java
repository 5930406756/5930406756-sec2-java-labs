package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import kamkrue.pisupa.lab4.Gender;
import kamkrue.pisupa.lab4.Patient;

public class PatientV2 extends Patient {
	public PatientV2(String name, String birthdate, Gender gender, double weight, int height) {
		super(name, birthdate, gender, weight, height);

	}

	public void patientReport() {
		System.out.println("You need medical attention.");
	}
}
package kamkrue.pisupa.lab5;

/**
 * @author Mr.Pisupa Kamkrue No.593040675-6 Section 2
 */
import java.time.temporal.ChronoUnit;
import kamkrue.pisupa.lab4.Gender;

public class OlderPatient {
	public static void main(String[] args) {
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "15.02.2000", Gender.MALE, 72, 172, "cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 68, 170, "Fire accident", true);
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);
	}

	public static void isOlder(PatientV2 firstPatient, PatientV2 secondPatient) {
		long daysBetween = ChronoUnit.DAYS.between(firstPatient.getBirthdate(), secondPatient.getBirthdate());
		if (daysBetween > 0) {
			System.out.println(firstPatient.getName()
					+ " is older than " + ((secondPatient instanceof VIPPatient)
							? (((VIPPatient) secondPatient).getVIPName("mickeymouse")) : secondPatient.getName())
					+ ".");
		} else {
			System.out.println(firstPatient.getName()
					+ " is not older than " + ((secondPatient instanceof VIPPatient)
							? (((VIPPatient) secondPatient).getVIPName("mickeymouse")) : secondPatient.getName())
					+ ".");
		}
	}

	public static void isOldest(PatientV2 firstPatient, PatientV2 secondPatient, PatientV2 thirdPatient) {
		long countDay = ChronoUnit.DAYS.between(firstPatient.getBirthdate(), secondPatient.getBirthdate());
		long countDay2 = ChronoUnit.DAYS.between(secondPatient.getBirthdate(), thirdPatient.getBirthdate());
		long countDay3 = ChronoUnit.DAYS.between(firstPatient.getBirthdate(), thirdPatient.getBirthdate());
		if (countDay > 0 && countDay3 > 0) {
			System.out
					.println(firstPatient.getName() + " is the oldest among " + firstPatient.getName() + ", "
							+ secondPatient.getName() + " and "
							+ ((thirdPatient instanceof VIPPatient)
									? ((VIPPatient) thirdPatient).getVIPName("mickeymouse") : thirdPatient.getName())
							+ ".");

		}
		if (countDay2 < 0 && countDay3 < 0) {
			System.out.println(thirdPatient.getName() + " is the oldest among " + firstPatient.getName()
					+ ", " + ((secondPatient instanceof VIPPatient)
							? ((VIPPatient) secondPatient).getVIPName("mickeymouse") : secondPatient.getName())
					+ " and " + thirdPatient.getName() + ".");
		}
	}
}

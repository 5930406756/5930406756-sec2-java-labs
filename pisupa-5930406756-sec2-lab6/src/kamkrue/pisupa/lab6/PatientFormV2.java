package kamkrue.pisupa.lab6;

/*
 *@author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import javax.swing.*;
import java.awt.*;

public class PatientFormV2 extends PatientFormV1 {

	private static final long serialVersionUID = 876704032860739322L;
	protected JPanel genderPanel, windowPanel, infomationPanel, subGenderPanel, genderAndAddressPanel, addressPanel;
	protected JLabel genderLabel;
	protected JRadioButton male, female;
	protected ButtonGroup groupGender;
	protected JTextArea textAddress;
	protected JScrollPane scrollPane;

	public PatientFormV2(String string) {
		super(string);
	}

	protected void addComponents() {
		super.addComponents();
		genderPanel = new JPanel();
		windowPanel = new JPanel();
		infomationPanel = super.panelOfPFV1;
		subGenderPanel = new JPanel();
		genderAndAddressPanel = new JPanel();
		addressPanel = new JPanel();
		genderLabel = new JLabel("Gender:");
		male = new JRadioButton("Male");
		female = new JRadioButton("Female");
		groupGender = new ButtonGroup();
		textAddress = new JTextArea(2, 35);
		scrollPane = new JScrollPane(textAddress);
		JLabel addressLabel = new JLabel("Adderess:");

		groupGender.add(male);
		groupGender.add(female);
		subGenderPanel.add(male);
		subGenderPanel.add(female);

		textAddress.setLineWrap(true);
		textAddress.setWrapStyleWord(true);
		textAddress.setText("Department of Computer Engineering, Facualty of Engineer, ");
		textAddress.append("Khon Kean University, Mittraparp Rd., T. Naimuang, A. Muang, Khon Kean, Thailand, 40002.");
		this.addressPanel.setLayout(new GridLayout(2, 1));
		this.addressPanel.add(addressLabel);
		this.addressPanel.add(scrollPane);

		genderPanel.setLayout(new GridLayout(1, 2, 0, 0));
		genderPanel.add(genderLabel);
		genderPanel.add(subGenderPanel);

		genderAndAddressPanel.setLayout(new BorderLayout(0, -5));
		genderAndAddressPanel.add(genderPanel, BorderLayout.NORTH);
		genderAndAddressPanel.add(this.addressPanel, BorderLayout.SOUTH);

		windowPanel.setLayout(new BorderLayout(0, 2));
		windowPanel.add(panelOfPFV1, BorderLayout.NORTH);
		windowPanel.add(genderAndAddressPanel, BorderLayout.CENTER);
		windowPanel.add(panelOfMSW, BorderLayout.SOUTH);
		setContentPane(windowPanel);

	}

	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

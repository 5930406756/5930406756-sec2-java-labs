package kamkrue.pisupa.lab6;

/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import javax.swing.*;

public class PatientFormV3 extends PatientFormV2 {
	private static final long serialVersionUID = 1L;
	protected JPanel basicInformationPanel, typePanel, mixPanel;
	protected JLabel typeLabel;
	protected JComboBox<String> patientType;
	protected JMenuBar menu;
	protected JMenu menuFile, menuConfig;
	protected JMenuItem menuItem, open, save, exit, color, size;

	public PatientFormV3(String string) {
		super(string);
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		basicInformationPanel = new JPanel();
		typePanel = new JPanel();
		mixPanel = new JPanel();
		typeLabel = new JLabel("Type: ");
		patientType = new JComboBox<String>();
		menu = new JMenuBar();
		menuFile = new JMenu("File");
		menuConfig = new JMenu("Config");
		menuItem = new JMenuItem("New");
		open = new JMenuItem("Open");
		save = new JMenuItem("Save");
		exit = new JMenuItem("Exit");
		color = new JMenuItem("Color");
		size = new JMenuItem("Size");

		basicInformationPanel.setLayout(new BorderLayout());
		basicInformationPanel.add(super.panelOfPFV1, BorderLayout.NORTH);
		basicInformationPanel.add(super.genderAndAddressPanel, BorderLayout.SOUTH);
		patientType.addItem("Inpatient");
		patientType.addItem("Outpatient");
		patientType.setEditable(true);
		typePanel.setLayout(new GridLayout(1, 2));
		typePanel.add(typeLabel);
		typePanel.add(patientType);
		menu.add(menuFile);
		menu.add(menuConfig);
		menuFile.add(menuItem);
		menuFile.add(open);
		menuFile.add(save);
		menuFile.add(exit);
		menuConfig.add(color);
		menuConfig.add(size);
		mixPanel.setLayout(new BorderLayout(0, 5));
		mixPanel.add(basicInformationPanel, BorderLayout.NORTH);
		mixPanel.add(typePanel, BorderLayout.CENTER);
		mixPanel.add(panelOfMSW, BorderLayout.SOUTH);
		this.setJMenuBar(menu);
		add(mixPanel);
	}

	public static void createAndShowGUI() {
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

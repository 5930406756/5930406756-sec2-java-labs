package kamkrue.pisupa.lab6;

/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import javax.swing.*;
import java.awt.*;

public class MySimpleWindow extends JFrame {

	private static final long serialVersionUID = -5367531674962028618L;
	protected JButton okButton = new JButton("OK");
	protected JButton cancelButton = new JButton("Cancel");
	protected JPanel panelOfMSW = new JPanel();

	public MySimpleWindow(String string) {
		super(string);
	}

	protected void addComponents() {
		panelOfMSW.add(cancelButton);
		panelOfMSW.add(okButton);
		add(panelOfMSW, BorderLayout.SOUTH);
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

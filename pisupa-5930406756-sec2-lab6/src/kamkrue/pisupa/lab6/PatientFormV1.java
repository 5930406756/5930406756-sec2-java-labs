package kamkrue.pisupa.lab6;

/*
 * @author Pisupa Kamkrue No.593040675-6 Sec.2
 */
import java.awt.*;
import javax.swing.*;

public class PatientFormV1 extends MySimpleWindow {

	private static final long serialVersionUID = -724486577254289631L;
	protected JLabel nameLabel, heightLabel, weightLabel, brithdateLabel;
	protected JTextField nameTextField, heightTextField, weightTextField, brithdateTextField;
	protected JPanel panelOfPFV1, mixPanel, textPanel, labelPanel, panelText;
	protected final static int TEXT_LENGTH = 15;

	public PatientFormV1(String string) {
		super(string);
	}

	protected void addComponents() {
		super.addComponents();
		panelOfPFV1 = new JPanel();

		nameLabel = new JLabel("Name:");
		heightLabel = new JLabel("Height (metre):");
		weightLabel = new JLabel("Weight (kg.):");
		brithdateLabel = new JLabel("Birthdate:");

		nameTextField = new JTextField(TEXT_LENGTH);
		heightTextField = new JTextField(TEXT_LENGTH);
		weightTextField = new JTextField(TEXT_LENGTH);
		brithdateTextField = new JTextField(TEXT_LENGTH);

		panelOfPFV1.setLayout(new GridLayout(4, 2, 0, 0));

		panelOfPFV1.add(nameLabel);
		panelOfPFV1.add(nameTextField);

		panelOfPFV1.add(brithdateLabel);
		panelOfPFV1.add(brithdateTextField);
		panelOfPFV1.add(heightLabel);
		panelOfPFV1.add(heightTextField);
		brithdateTextField.setToolTipText("ex. 22.02.2000");
		panelOfPFV1.add(weightLabel);
		panelOfPFV1.add(weightTextField);

		mixPanel = new JPanel(new BorderLayout());

		mixPanel.add(panelOfMSW, BorderLayout.SOUTH);
		mixPanel.add(panelOfPFV1, BorderLayout.NORTH);
		setContentPane(mixPanel);
	}

	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
